# MIT License

# Copyright (c) 2018-2019 Groupe Allo-Media

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import Dict, Optional, Set, Tuple, List

from .base import Language

#
# CONSTANTS
# Built once on import.
#

# Those words multiplies lesser numbers (see Rules)
# Special case: "hundred" is processed apart.
MULTIPLIERS = {
    "tausend": 1_000,
    "tausende": 1_000,
    "million": 1_000_000,
    "millionen": 1_000_000,
    "milliarde": 1_000_000_000_000,
    "milliarden": 1_000_000_000_000,
}


# Units are terminals (see Rules)
# Special case: "zero/O" is processed apart.
UNITS: Dict[str, int] = {
    word: value
    for value, word in enumerate(
        "eins zwei drei vier fünf sechs sieben acht neun".split(), 1
    )
}


# Single tens are terminals (see Rules)
STENS: Dict[str, int] = {
    word: value
    for value, word in enumerate(
        "zehn elf zwölf dreizehn vierzehn fünfzehn sechzehn siebzehn achtzehn neunzehn".split(),
        10,
    )
}

# Ten multiples
# Ten multiples may be followed by a unit only;
MTENS: Dict[str, int] = {
    word: value * 10
    for value, word in enumerate(
        "zwanzig dreißig vierzig fünfzig sechzig siebzig achtzig neunzig".split(), 2
    )
}

# Ten multiples that can be combined with STENS
MTENS_WSTENS: Set[str] = set()


# "hundred" has a special status (see Rules)
HUNDRED = {"hundert": 100, "hunderte": 100}


# Composites are tens already composed with terminals in one word.
# Composites are terminals.

COMPOSITES: Dict[str, int] = {
    "und".join((unit_word, ten_word)): ten_val + unit_val
    for ten_word, ten_val in MTENS.items()
    for unit_word, unit_val in UNITS.items()
}

COMPOSITES_HUNDRED: Dict[str, int] = {
    "".join((unit_word, hundred_word)): unit_val * hundred_val
    for unit_word, unit_val in UNITS.items()
    for hundred_word, hundred_val in HUNDRED.items()
}

COMPOSITES_THOUSAND: Dict[str, int] = {
    "".join((unit_word, thousand_word)): unit_val * thousand_val
    for unit_word, unit_val in UNITS.items()
    for thousand_word, thousand_val in [("tausend", 1000)]
}

COMPOSITES.update(COMPOSITES_HUNDRED)
COMPOSITES.update(COMPOSITES_THOUSAND)

MULTIPLIERS.update(COMPOSITES_THOUSAND)
HUNDRED.update(COMPOSITES_HUNDRED)

# All number words

NUMBERS = MULTIPLIERS.copy()
NUMBERS.update(UNITS)
NUMBERS.update(STENS)
NUMBERS.update(MTENS)
NUMBERS.update(HUNDRED)
NUMBERS.update(COMPOSITES)


class German(Language):

    ADDITIONS: Dict[str, int] = {}
    MULTIPLIERS = MULTIPLIERS
    UNITS = UNITS
    STENS = STENS
    MTENS = MTENS
    MTENS_WSTENS = MTENS_WSTENS
    HUNDRED = HUNDRED
    NUMBERS = NUMBERS

    SIGN = {"plus": "+", "minus": "-"}
    ZERO = {"null", "o"}
    DECIMAL_SEP = "punkt"
    DECIMAL_SYM = "."

    AND_NUMS: Set[str] = set()
    AND = "und"
    NEVER_IF_ALONE = {"ein", "eine", "eins"}

    # Relaxed composed numbers (two-words only)
    # start => (next, target)
    RELAXED: Dict[str, Tuple[str, str]] = {}

    def ord2card(self, word: str) -> Optional[str]:
        """Convert ordinal number to cardinal.

        Return None if word is not an ordinal or is better left in letters
        as is the case for fist and second.
        """

        if word.endswith("sten") or word.endswith("ster"):
            source = word[:-4]
        elif word.endswith("zuerst"):
            source = word.replace("zuerst", "ein")
        else:
            return None

        if source not in self.NUMBERS:
            return None

        return source

    def num_ord(self, digits: str, original_word: str) -> str:
        """Add suffix to number in digits to make an ordinal"""
        if (original_word.endswith("ten") or original_word.endswith("ter")):
            sf = "s"
        else:
            sf = ""
        return f"{digits}{sf}"

    def normalize(self, word: str) -> str:
        return word

    def tokenize(self, sent: str) -> List[str]:
        "Split word with hundert"
        items = sent.split()
        for i, item in enumerate(items):
            if "hundert" in item:
                parts = item.split("hundert")
                if len(parts) == 2 and parts[-1] != "ste" and parts[-1] != "en":
                    items[i] = "hundert ".join(parts)
        return " ".join(items).split()
