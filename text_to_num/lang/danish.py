# MIT License

# Copyright (c) 2018-2019 Groupe Allo-Media

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import Dict, Optional, Set, Tuple

from .base import Language

#
# CONSTANTS
# Built once on import.
#

# Those words multiplies lesser numbers (see Rules)
# Special case: "hundred" is processed apart.
MULTIPLIERS = {
    "tusind": 1_000,
    "tusinder": 1_000,
    "million": 1_000_000,
    "millioner": 1_000_000,
    "milliard": 1_000_000_000,
    "milliarder": 1_000_000_000,
}


# Units are terminals (see Rules)
# Special case: "zero/O" is processed apart.
UNITS: Dict[str, int] = {
    word: value
    for value, word in enumerate(
        "en to tre fire fem seks syv otte otte ni".split(), 1
    )
}

# Single tens are terminals (see Rules)
STENS: Dict[str, int] = {
    word: value
    for value, word in enumerate(
        "ti elleve tolv tretten fjorten femten seksten sytten atten nitten".split(),
        10,
    )
}

# Ten multiples
# Ten multiples may be followed by a unit only;
MTENS: Dict[str, int] = {
    word: value * 10
    for value, word in enumerate(
        "tyve tredive fyrre halvtreds tres halvfjerds firs halvfems".split(), 2
    )
}

# Ten multiples that can be combined with STENS
MTENS_WSTENS: Set[str] = set()


# "hundred" has a special status (see Rules)
HUNDRED = {"hundrede": 100, "hundreder": 100}


# Composites are tens already composed with terminals in one word.
# Composites are terminals.

COMPOSITES: Dict[str, int] = {
    "og".join((unit_word, ten_word)): ten_val + unit_val
    for ten_word, ten_val in MTENS.items()
    for unit_word, unit_val in UNITS.items()
}

# All number words

NUMBERS = MULTIPLIERS.copy()
NUMBERS.update(UNITS)
NUMBERS.update(STENS)
NUMBERS.update(MTENS)
NUMBERS.update(HUNDRED)
NUMBERS.update(COMPOSITES)


class Danish(Language):

    ADDITIONS: Dict[str, int] = {}
    MULTIPLIERS = MULTIPLIERS
    UNITS = UNITS
    STENS = STENS
    MTENS = MTENS
    MTENS_WSTENS = MTENS_WSTENS
    HUNDRED = HUNDRED
    NUMBERS = NUMBERS

    SIGN = {"plus": "+", "minus": "-"}
    ZERO = {"nul", "o"}
    DECIMAL_SEP = "punkt"
    DECIMAL_SYM = "."

    AND_NUMS: Set[str] = set(MTENS.keys())
    AND = "og"
    NEVER_IF_ALONE = {"et", "en"}

    # Relaxed composed numbers (two-words only)
    # start => (next, target)
    RELAXED: Dict[str, Tuple[str, str]] = {}

    def ord2card(self, word: str) -> Optional[str]:
        """Convert ordinal number to cardinal.

        Return None if word is not an ordinal or is better left in letters
        as is the case for fist and second.
        """

        plur_suff = word.endswith("de")
        if not plur_suff:
            if word.endswith("første"):
                source = word.replace("første", "en")
            elif word.endswith("anden"):
                source = word.replace("anden", "to")
            elif word.endswith("tredje"):
                source = word.replace("tredje", "tre")
            elif word.endswith("fjerde"):
                source = word.replace("fjerde", "fire")
            elif word.endswith("femte"):
                source = word.replace("femte", "fem")
            elif word.endswith("sjette"):
                source = word.replace("sjette", "seks")
            elif word.endswith("ellevte"):
                source = word.replace("ellevte", "elleve")
            elif word.endswith("tolvte"):
                source = word.replace("tolvte", "tolv")
            else:
                return None
        else:
            if word.endswith("syvende"):
                source = word.replace("syvende", "syv")
            elif word.endswith("ottende"):
                source = word.replace("ottende", "otte")
            elif word.endswith("niende"):
                source = word.replace("niende", "ni")
            elif word.endswith("tiende"):
                source = word.replace("tiende", "ti")
            else:
                source = word[:-2]

        if source not in self.NUMBERS:
            return None

        return source

    def num_ord(self, digits: str, original_word: str) -> str:
        """Add suffix to number in digits to make an ordinal"""
        if original_word.endswith("e"):
            sf = "e"
        else:
            sf = ""
        return f"{digits}{sf}"

    def normalize(self, word: str) -> str:
        return word
