# MIT License

# Copyright (c) 2018-2019 Groupe Allo-Media

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import Dict, Optional, Set, Tuple

from .base import Language

#
# CONSTANTS
# Built once on import.
#

# Those words multiplies lesser numbers (see Rules)
# Special case: "hundred" is processed apart.
MULTIPLIERS = {
    "الاف": 1_000,
    "آلاف": 1_000,
    "ملايين": 1_000_000,
}


# Units are terminals (see Rules)
# Special case: "zero/O" is processed apart.
UNITS: Dict[str, int] = {
    "واحد": 1,
    "اثنان": 2,
    "ثلاثة": 3,
    "أربعة": 4,
    "خمسة": 5,
    "ستة": 6,
    "سبعة": 7,
    "ثمانية": 8,
    "تسعة": 9,
    "ثلاث": 3,
    "خمس": 5,
    "ست": 6,
    "تسع": 9,
}

ADDITIONS: Dict[str, int] = {
    "عشرة": 10,
    "عشر": 10,
    "عشرون": 20,
    "ثلاثون": 30,
    "اربعون": 40,
    "خمسون": 50,
    "ستون": 60,
    "سبعون": 70,
    "سبعين": 70,
    "ثمانون": 80,
    "تسعون": 90,
}

STENS: Dict[str, int] = {}

MTENS = ADDITIONS

# Ten multiples that can be combined with STENS
MTENS_WSTENS: Set[str] = set()

# "hundred" has a special status (see Rules)
HUNDRED = {
  "مائة": 100,
  "مائتين": 200,
  "ثلاثمائه": 300,
  "أربعةمئة": 400,
  "خمسمائة": 500,
  "ستمائة": 600,
  "سبعمائة": 700,
  "ثمانمائة": 800,
  "تسعمائة": 900,
}


# All number words

NUMBERS = MULTIPLIERS.copy()
NUMBERS.update(UNITS)
NUMBERS.update(ADDITIONS)
NUMBERS.update(MTENS)
NUMBERS.update(HUNDRED)


class Arabic(Language):

    ADDITIONS = ADDITIONS
    MULTIPLIERS = MULTIPLIERS
    UNITS = UNITS
    STENS = STENS
    MTENS = MTENS
    MTENS_WSTENS = MTENS_WSTENS
    HUNDRED = HUNDRED
    NUMBERS = NUMBERS

    SIGN = {
        "أكثر": "+",
        "ناقص": "-"
    }
    ZERO = {"صفر"}
    DECIMAL_SEP = "فاصلة"
    DECIMAL_SYM = "."

    AND_NUMS: Set[str] = set()
    AND = "و"
    NEVER_IF_ALONE = {"واحد"}

    # Relaxed composed numbers (two-words only)
    # start => (next, target)
    RELAXED: Dict[str, Tuple[str, str]] = {}

    def ord2card(self, word: str) -> Optional[str]:
        """Convert ordinal number to cardinal.

        Return None if word is not an ordinal or is better left in letters
        as is the case for fist and second.
        """

        # Arabic rules are pretty complex for ordinals and require
        # word reordering (5th house -> house fifth), skip them for now
        return None

    def num_ord(self, digits: str, original_word: str) -> str:
        """Add suffix to number in digits to make an ordinal"""
        return f"{digits}"

    def normalize(self, word: str) -> str:
        if word[0] == "و" and word[1:] in NUMBERS:
            word = word[1:]
        return word
