# MIT License

# Copyright (c) 2018-2019 Groupe Allo-Media

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import Dict, Optional, Set, Tuple

from .base import Language

#
# CONSTANTS
# Built once on import.
#

# Those words multiplies lesser numbers (see Rules)
# Special case: "hundred" is processed apart.
MULTIPLIERS = {
    "tusen": 1_000,
    "tusentals": 1_000,
    "miljon": 1_000_000,
    "miljoner": 1_000_000,
    "miljard": 1_000_000_000,
    "miljarder": 1_000_000_000,
    "biljon": 1_000_000_000_000,
    "biljoner": 1_000_000_000_000,
}


# Units are terminals (see Rules)
# Special case: "zero/O" is processed apart.
UNITS: Dict[str, int] = {
    word: value
    for value, word in enumerate(
        "ett två tre fyra fem sex sju åtta nio".split(), 1
    )
}
UNITS["en"] = 1

# Single tens are terminals (see Rules)
STENS: Dict[str, int] = {
    word: value
    for value, word in enumerate(
        "tio elva tolv tretton fjorton femton sjutton sexton arton nitton".split(),
        10,
    )
}

# Ten multiples
# Ten multiples may be followed by a unit only;
MTENS: Dict[str, int] = {
    word: value * 10
    for value, word in enumerate(
        "tjugo trettio fyrtio femtio sextio sjuttio åttio nittio".split(), 2
    )
}

# Ten multiples that can be combined with STENS
MTENS_WSTENS: Set[str] = set()


# "hundred" has a special status (see Rules)
HUNDRED = {"hundra": 100, "hundratals": 100}


# Composites are tens already composed with terminals in one word.
# Composites are terminals.

COMPOSITES: Dict[str, int] = {
    "-".join((ten_word, unit_word)): ten_val + unit_val
    for ten_word, ten_val in MTENS.items()
    for unit_word, unit_val in UNITS.items()
}

# All number words

NUMBERS = MULTIPLIERS.copy()
NUMBERS.update(UNITS)
NUMBERS.update(STENS)
NUMBERS.update(MTENS)
NUMBERS.update(HUNDRED)
NUMBERS.update(COMPOSITES)


class Swedish(Language):

    ADDITIONS: Dict[str, int] = {}
    MULTIPLIERS = MULTIPLIERS
    UNITS = UNITS
    STENS = STENS
    MTENS = MTENS
    MTENS_WSTENS = MTENS_WSTENS
    HUNDRED = HUNDRED
    NUMBERS = NUMBERS

    SIGN = {"plus": "+", "minus": "-"}
    ZERO = {"noll", "o"}
    DECIMAL_SEP = "punkt"
    DECIMAL_SYM = "."

    AND_NUMS: Set[str] = set()
    AND = "och"
    NEVER_IF_ALONE = {"ett"}

    # Relaxed composed numbers (two-words only)
    # start => (next, target)
    RELAXED: Dict[str, Tuple[str, str]] = {}

    def ord2card(self, word: str) -> Optional[str]:
        """Convert ordinal number to cardinal.

        Return None if word is not an ordinal or is better left in letters
        as is the case for fist and second.
        """

        plur_suff = word.endswith("nde")
        if not plur_suff:
            if word.endswith("första"):
                source = word.replace("första", "ett")
            elif word.endswith("andra"):
                source = word.replace("andra", "två")
            elif word.endswith("tredje"):
                source = word.replace("tredje", "tre")
            elif word.endswith("fjärde"):
                source = word.replace("fjärde", "fyra")
            elif word.endswith("femte"):
                source = word.replace("femte", "fem")
            elif word.endswith("sjätte"):
                source = word.replace("sjätte", "sex")
            elif word.endswith("åttonde"):
                source = word.replace("åttonde", "åtta")

            elif word.endswith("elfte"):
                source = word.replace("elfte", "elva")
            elif word.endswith("tolfte"):
                source = word.replace("tolfte", "tolv")
            else:
                return None
        else:
            if word.endswith("åttonde"):
                source = word.replace("åttonde", "åtta")
            elif word.endswith("tonde"):
                source = word.replace("tonde", "ton")
            else:
                source = word[:-3]

        if source not in self.NUMBERS:
            return None

        return source

    def num_ord(self, digits: str, original_word: str) -> str:
        """Add suffix to number in digits to make an ordinal"""
        if original_word.endswith("e"):
            sf = "e"
        else:
            sf = ""
        return f"{digits}{sf}"

    def normalize(self, word: str) -> str:
        return word
