# MIT License

# Copyright (c) 2018-2019 Groupe Allo-Media

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import Dict, Optional, Set, Tuple

from .base import Language

#
# CONSTANTS
# Built once on import.
#

# Those words multiplies lesser numbers (see Rules)
# Special case: "hundred" is processed apart.
MULTIPLIERS = {
    "labing": 10,
    "libo": 1_000,
    "milyon": 1_000_000,
    "angaw": 1_000_000,
    "bilyon": 1_000_000_000,
}


# Units are terminals (see Rules)
# Special case: "zero/O" is processed apart.
UNITS: Dict[str, int] = {
    word: value
    for value, word in enumerate(
        "isa dalawa tatlo apat lima anim pitong walo siyam".split(), 1
    )
}

# Single tens are terminals (see Rules)
STENS: Dict[str, int] = {
    word: value
    for value, word in enumerate(
        "sampu labingisang labindalawa labintatlo labingapat "
        "labinlimang labinganim labingpitong labingwalo labinsiyam".split(),
        10,
    )
}

# Ten multiples
# Ten multiples may be followed by a unit only;
MTENS: Dict[str, int] = {
    word: value * 10
    for value, word in enumerate(
        "dalawampu tatlumpu apatnapu limampu animnapu pitumpu walumpu siyamnapung".split(), 2
    )
}

# Ten multiples that can be combined with STENS
MTENS_WSTENS: Set[str] = set()


# "hundred" has a special status (see Rules)
HUNDRED = {"daan": 100, "daang": 100}


# Composites are tens already composed with terminals in one word.
# Composites are terminals.

COMPOSITES: Dict[str, int] = {
    "-".join((ten_word, unit_word)): ten_val + unit_val
    for ten_word, ten_val in MTENS.items()
    for unit_word, unit_val in UNITS.items()
}

# All number words

NUMBERS = MULTIPLIERS.copy()
NUMBERS.update(UNITS)
NUMBERS.update(STENS)
NUMBERS.update(MTENS)
NUMBERS.update(HUNDRED)
NUMBERS.update(COMPOSITES)

ORDMAP = {
    "una": "isa",
    "unang": "isa",
    "pangalawa": "dalawa",
    "ikalawa": "dalawa",
    "pangatlo": "tatlo",
    "ikatlo": "tatlo",
}


class Filipino(Language):

    ADDITIONS: Dict[str, int] = {}
    MULTIPLIERS = MULTIPLIERS
    UNITS = UNITS
    STENS = STENS
    MTENS = MTENS
    MTENS_WSTENS = MTENS_WSTENS
    HUNDRED = HUNDRED
    NUMBERS = NUMBERS

    SIGN = {"plus": "+", "bawas": "-"}
    ZERO = {"zero"}
    DECIMAL_SEP = "punto"
    DECIMAL_SYM = "."

    AND_NUMS: Set[str] = set()
    AND = "at"
    NEVER_IF_ALONE = {"isa"}

    # Relaxed composed numbers (two-words only)
    # start => (next, target)
    RELAXED: Dict[str, Tuple[str, str]] = {}

    def ord2card(self, word: str) -> Optional[str]:
        """Convert ordinal number to cardinal.

        Return None if word is not an ordinal or is better left in letters
        as is the case for fist and second.
        """

        # not very complete list probably
        if word in ORDMAP:
            return ORDMAP[word]

        if word[0:3] == "pan" and word[3:] in NUMBERS:
            return word[3:]

        if word[0:3] == "ika" and word[3:] in NUMBERS:
            return word[3:]

        return None

    def num_ord(self, digits: str, original_word: str) -> str:
        """Add suffix to number in digits to make an ordinal"""

        return f"{digits}"

    def normalize(self, word: str) -> str:
        if len(word) > 2 and word[-2:] == "ng" and word[0:-2] in NUMBERS:
            word = word[:-2]
        if len(word) > 2 and word[-2:] == "'t" and word[0:-2] in NUMBERS:
            word = word[:-2]
        return word
